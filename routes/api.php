<?php

use App\Http\Controllers\Api\Admin\Item\ItemController as AdminItemController;
use App\Http\Controllers\Api\Player\Item\ItemController as PlayerItemController;
use App\Http\Controllers\Api\Player\Cart\CartController as PlayerCartController;
use App\Http\Controllers\Api\Player\Item\ItemTypeController as PlayerItemTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/admin')->group(function () {
    Route::get('/types-with-items', [AdminItemController::class, 'getTypesWithItems']);
    Route::get('/{item}/craft', [AdminItemController::class, 'getItemCraft']);
    Route::apiResource('/items', AdminItemController::class);
});

Route::prefix('/player')->group(function () {
    Route::get('/types-with-items', [PlayerItemController::class, 'getTypesWithItems']);
    Route::get('/item-types', [PlayerItemTypeController::class, 'getItemTypes']);

    Route::post('/add-to-cart', [PlayerCartController::class, 'addToCart']);
    Route::get('/user-cart/{user}', [PlayerCartController::class, 'get']);
    Route::delete('/user-cart/delete/{recordId}', [PlayerCartController::class, 'deleteItem']);
    Route::get('/get-cart-count/{user}', [PlayerCartController::class, 'cartItemCount']);
});
