<?php

use App\Http\Controllers\Admin\Item\ItemController as AdminItemController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Player\Cart\CartController as PlayerCartController;
use App\Http\Controllers\Player\Item\ItemController as PlayerItemController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

//--Admin---------------------------------------------------------------------------------------------------------------
Route::prefix('/admin/items')->middleware(['auth', 'role:admin'])->name('admin.items.')->group(function () {
    Route::get('/', [AdminItemController::class, 'index'])->name('index');
    Route::get('/add', [AdminItemController::class, 'add'])->name('add');
    Route::get('/list', [AdminItemController::class, 'list'])->name('list');
});

//--Player--------------------------------------------------------------------------------------------------------------
Route::prefix('/player')->middleware(['auth', 'role:player'])->name('player.')->group(function () {
    Route::get('/shopping-cart/', [PlayerCartController::class, 'index'])->name('shopping-cart.index');
    Route::prefix('/items')->name('items.')->group(function () {
        Route::get('/', [PlayerItemController::class, 'index'])->name('index');
    });
});
