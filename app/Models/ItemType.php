<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function items()
    {
        return $this->hasMany(Item::class, 'type_id', 'id');
    }
}
