<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function type()
    {
        return $this->belongsTo(ItemType::class, 'type_id');
    }

    public function craftsFrom()
    {
        return $this->belongsToMany(Item::class, 'crafts', 'item_id', 'item_for_crafting_id')
            ->withPivot(['position']);
    }

    public function usedInCrafts()
    {
        return $this->belongsToMany(Item::class, 'crafts', 'item_for_crafting_id', 'item_id');
    }

}
