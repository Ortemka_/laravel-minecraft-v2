<?php

namespace App\Http\Controllers\Player\Item;

use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function index()
    {
        return view('player.items.index');
    }
}
