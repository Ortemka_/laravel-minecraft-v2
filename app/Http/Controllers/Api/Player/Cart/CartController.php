<?php

namespace App\Http\Controllers\Api\Player\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\User;
use Illuminate\Http\Request;
use function response;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        $data = $request->all();

        $cart = Cart::create($data);

        return $cart;
    }

    public function cartItemCount(User $user)
    {
        $count = $user->cart->count();

        return $count;
    }

    public function get(User $user)
    {
        return $user->cart()->with('item')->get();
    }

    public function deleteItem(string $recordId)
    {
        Cart::destroy($recordId);

        return response([]);
    }
}
