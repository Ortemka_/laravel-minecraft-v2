<?php

namespace App\Http\Controllers\Api\Player\Item;

use App\Http\Controllers\Controller;
use App\Models\ItemType;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function getTypesWithItems()
    {
        $typesWithItems = ItemType::with('items')->get();

        return $typesWithItems;
    }
}
