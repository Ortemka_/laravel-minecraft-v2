<?php

namespace App\Http\Controllers\Api\Player\Item;

use App\Models\ItemType;

class ItemTypeController
{

    public function getItemTypes() {
        $itemTypes = ItemType::all();
        return $itemTypes;
    }
}
