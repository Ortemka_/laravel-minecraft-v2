<?php

namespace App\Http\Controllers\Api\Admin\Item;

use App\Http\Controllers\Controller;
use App\Http\Requests\Item\UpdateRequest;
use App\Http\Resources\Item\ItemResource;
use App\Models\Item;
use App\Models\ItemType;
use Illuminate\Http\Request;


class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Item::all();

        return ItemResource::collection($items);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $itemData = $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'type_id' => 'required|integer',
            'image' => ['required', 'image:jpg, jpeg, png'],
        ]);

        $imageName = $request->file('image')->getClientOriginalName();
        $image_path = $request->file('image')->storeAs('items', $imageName);

        $itemData['image'] = $image_path;

        $craftData = [];
        foreach ($request->all() as $formKey => $value) {
            if (str_starts_with($formKey, 'position')) {
                $craftData[] = [
                    'position' => substr($formKey, -1),
                    'item_for_crafting_id' => $value
                ];
            }
        }

        $item = Item::create($itemData);

        $item->craftsFrom()->sync($craftData);

        return response([]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Item $item)
    {
        $data = $request->validated();
        $item->update($data);

        return $item;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Item $item)
    {
        $item->delete();

        return response([]);
    }

    public function getTypesWithItems()
    {
        $typesWithItems = ItemType::with('items')->get();

        return $typesWithItems;
    }

    public function getItemCraft(Item $item)
    {
        $craft = $item->craftsFrom;

        return $craft;
    }

}
