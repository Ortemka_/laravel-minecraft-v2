<?php

namespace App\Http\Controllers\Admin\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;


class ItemController extends Controller
{
    public function index()
    {
        return view('admin.items.index');
    }

    public function add()
    {
        return view('admin.items.add');
    }

    public function list()
    {
        return view('admin.items.list');
    }

}
