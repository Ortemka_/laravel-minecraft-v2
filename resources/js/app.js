/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import {createApp} from 'vue';

/**
 * Next, we will create a fresh Vue application instance. You may then begin
 * registering components with the application instance so they are ready
 * to use in your application's views. An example is included for you.
 */

const app = createApp({});

import CartCounterComponent from "./components/player/cart/CartCounterComponent.vue";
import CartComponent from "./components/player/cart/CartComponent.vue";

import ItemsByTypeComponent from "./components/admin/item/ItemsByTypeComponent.vue";
import CreateItemComponent from "./components/admin/item/CreateItemComponent.vue";
import ItemsTableComponent from "./components/admin/item/ItemsTableComponent.vue";

import PlayerItemsByTypeComponent from "./components/player/item/ItemsByTypeComponent.vue";

// Player - Item  management components
app.component('player-items-by-type-component', PlayerItemsByTypeComponent);

// Player Cart components
app.component('cart-counter-component', CartCounterComponent);
app.component('cart-component', CartComponent);

// Admin - Item  management components
app.component('items-by-type-component', ItemsByTypeComponent);
app.component('create-item-component', CreateItemComponent)
app.component('item-table-component', ItemsTableComponent)


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// Object.entries(import.meta.glob('./**/*.vue', { eager: true })).forEach(([path, definition]) => {
//     app.component(path.split('/').pop().replace(/\.\w+$/, ''), definition.default);
// });

/**
 * Finally, we will attach the application instance to a HTML element with
 * an "id" attribute of "app". This element is included with the "auth"
 * scaffolding. Otherwise, you will need to add an element yourself.
 */

app.mount('#app');
