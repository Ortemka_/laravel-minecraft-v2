@extends('layouts.app')

@section('content')
    <cart-component user-id="{{ auth()->user()->id }}"></cart-component>
@endsection
