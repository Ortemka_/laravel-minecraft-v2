@extends('layouts.app')

@section('content')
    @include('player.items.sub-navigation')
    <player-items-by-type-component user-id="{{ auth()->user()->id }}"></player-items-by-type-component>
@endsection
