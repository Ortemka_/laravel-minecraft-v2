<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a href="{{ route('player.items.index') }}" class="nav-link ms-2 {{ request()->routeIs('player.items.index') ? 'btn border-success' : '' }}">All items</a>
        </div>
    </div>
</nav>
