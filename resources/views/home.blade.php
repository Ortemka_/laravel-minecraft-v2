@extends('layouts.app')

@section('content')

    <table class="table w-25">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
        </tr>
        </thead>
        <tbody>
        @foreach($itemTypes as $type)
        <tr>
            <td>{{ $type->id }}</td>
            <td>{{ $type->title }}</td>
        </tr>
        @endforeach

        </tbody>
    </table>

@endsection
