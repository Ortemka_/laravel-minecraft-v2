@extends('layouts.app')

@section('content')
    @include('admin.items.sub-navigation')
    <items-by-type-component user-id="{{ auth()->user()->id }}"></items-by-type-component>
@endsection
