<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a href="{{ route('admin.items.index') }}" class="nav-link ms-2 {{ request()->routeIs('admin.items.index') ? 'btn border-success' : '' }}">All items</a>
            <a href="{{ route('admin.items.add') }}" class="nav-link ms-2 {{ request()->routeIs('admin.items.add') ? 'btn border-success' : '' }}">Add item</a>
            <a href="{{ route('admin.items.list') }}" class="nav-link ms-2 {{ request()->routeIs('admin.items.list') ? 'btn border-success' : '' }}">Items list</a>
        </div>
    </div>
</nav>
