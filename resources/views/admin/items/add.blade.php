@extends('layouts.app')

@section('content')
    @include('admin.items.sub-navigation')
    <create-item-component></create-item-component>
@endsection
