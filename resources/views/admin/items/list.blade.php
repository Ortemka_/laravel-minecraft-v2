@extends('layouts.app')

@section('content')
    @include('admin.items.sub-navigation')
    <item-table-component></item-table-component>
@endsection
